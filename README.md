# Jailbreak no Kindle

Há algumas semanas eu [postei no twitter][twitter-book-cover] que eu havia feito jailbreak no Kindle pra que ele mostrasse a capinha do livro que eu estou lendo quando a tela ficasse travada. (Depois disso eu acabei mudando pra [imagens fixas][twitter-fixed-images] mesmo). O [Ygor][ygor] [e][barbara] [mais][filipegamer] [uma][centralheaven] [galera][analuiza] ficaram interessados. 

Como eu prometi que ia escrever um guia (e talvez gravar um vídeo) eu vou fazer Jailbreak no Kindle da [Carol][csacerdote] e documentar os passos aqui.

### INFORMAÇÕES IMPORTANTES

 - **EU NÃO ME RESPONSABILIZO SE ISSO DER ERRADO E SEU KINDLE NÃO LIGAR MAIS**
 - Todo conteúdo do seu Kindle será apagado
 - Nem todos os Kindles/Firmwares são compatíveis com o processo
 - **EU NÃO ME RESPONSABILIZO SE ISSO DER ERRADO E SEU KINDLE NÃO LIGAR MAIS**

##  Identifique seu Kindle

Seguindo [esta lista][kindle-model-id], identifique o modelo do seu Kindle (e o *nickname* do mesmo). Eu fiz uma [ferramenta][kindled] pra te ajudar, é só colocar o número de série do seu Kindle, a versão do seu firmware e ela te dá todas as informações e os downloads necessários. Recomendo que você use a ferramenta em conjunto com este post.

**AS IMAGENS DE FÁBRICA NÃO TEM TRAVA DE ACORDO COM O MODELO. GARANTIR QUE O NÚMERO DE SÉRIE ESTÁ CORRETO É A ÚNICA PROTEÇÃO PRO SEU KINDLE!**

Os modelos suportados desse Jailbreak são os seguintes: (daqui pra frente sempre usarei o Nickname dos modelos pra identificar os mesmos)

 - [**KOA**](https://gitlab.com/filipekiss/kindle/raw/master/factory-images/update_KOA_5.7.4_initial.bin)
 - [**KV**](https://gitlab.com/filipekiss/kindle/raw/master/factory-images/update_KV_5.5.0_initial.bin)
 - [**KT3**](https://gitlab.com/filipekiss/kindle/raw/master/factory-images/update_KT3_5.8.0_initial.bin)
 - [**KT2**](https://gitlab.com/filipekiss/kindle/raw/master/factory-images/update_KT2_5.6.0_initial.bin)
 - [**PW3 / PW3W**](https://gitlab.com/filipekiss/kindle/raw/master/factory-images/update_PW3_5.7.4_initial.bin)
 - [**PW2**](https://gitlab.com/filipekiss/kindle/raw/master/factory-images/update_PW2_5.4.3.2_initial.bin)

### Atenção: O modelo **PW2** possui algumas diferenças em relação aos outros modelos que eu não tenho como testar. Vou apenas reproduzir as instruções, mas, como tudo nesse tutorial, é por sua conta e risco.

## Preparação Geral

 - Tenha certeza que seu Kindle foi ativado na Amazon antes de continuar. Algumas opções do menu só aparecem para Kindles ativados.
 - Certifique-se que não há nenhum tipo de trava dos pais ou senha no dispositivo
 - Tenha seu Kindle com, pelo menos, 50% de bateria. Não queremos surpresas no meio do caminho. Coloca esse Kindle pra carregar na tomada agora. Vai.
 - Coloque seu Kindle no Modo Avião durante todo o procedimento
 - Certifique-se de que seu Kindle e Firmware são compatíveis com o Jailbreak. Você pode (deve?) usar a [ferramenta][kindled] que eu citei acima.


## Passo a Passo

#### Reset seu Kindle

**Essa é a única vez que isso deve ser feito. Você repetir esse processo elimina o jailbreak e devolve o Kindle às configurações de fábrica.**

 - Na tela inicial do seu Kindle, toque no menu de opções no canto superior direito **[Menu]** > **[Configurações]** > **[Menu]** > **[Redefinir Dispositivo]**.
 - Na janela que aparece, toque em **[Sim]**.
 - Configure e ative seu Kindle normalmente, como se você tivesse acabado de retirar ele da caixa e fosse usar. Ative ele na Amazon e vá até você chegar na tela inicial.
 - Depois disso, coloque seu Kindle em **Modo Avião** e siga para o próximo passo.

#### Instalando o Firmware de fábrica no seu Kindle

Mais uma vez, **CONFIRA O MODELO DO SEU KINDLE E SE ELE É COMPATÍVEL COM O JAILBREAK**. O Firmware de fábrica não tem trava de modelo e instalar o firmware errado pode deixar seu Kindle inutilizável. Não diga que eu não avisei.

 - Localize o arquivo referente ao seu modelo (no meu caso é o `update_PW3_5.7.4_initial.bin`).
 - Conecte seu Kindle via USB e copie o arquivo pra pasta raiz do seu Kindle. (_Se você acabou de resetar o Kindle, ao conectar via USB, você verá uma pasta chamada `documents`. O arquivo do update deve ficar **JUNTO** dessa pasta e não DENTRO dela_)
 - Ejete o Kindle (Remova com Segurança)
 - Na tela inicial, **[Menu]** > **[Configurações]** > **[Menu]** > **[Atualizar seu Kindle]** > **[Ok]**. Aguarde até que o processo seja concluído. Aqui levou cerca de 10 minutos. Seu Kindle será reiniciando quando o processo for concluído.

#####  ATENÇÃO: Se você está utilizando um **Kindle PW2** a tela de atualização não será exibida. Seu Kindle parecerá travado e, após alguns minutos, a mensagem **** JAILBREAK **** vai aparecer na parte de baixo da sua tela e, após isso, seu jailbreak estará concluído. Não é necessário fazer os próximos dois passos.

#### Ativando o Firmware do Jailbreak

 - Baixe o [arquivo do firmware](https://gitlab.com/filipekiss/kindle/raw/master/jailbreak/main-htmlviewer.tar.gz)
 - Conecte seu Kindle via USB
 - Copie o arquivo pro diretório raiz do seu Kindle (mesmo diretório que usamos da outra vez)
 - Desconecte o Kindle (Ejetar/Remover com Segurança)
 - Na tela inicial do seu Kindle, clique na **Pesquisa** (localizada no topo da tela, ao lado do **[Menu]**
 - No campo de busca, digite (sim, começando com `;` mesmo):

 ```
 ;installHtml
 ``` 

 - Clique **↩** (Enter) para buscar
 - A tela vai piscar e seu kindle vai reiniciar. Um novo documento vai aparecer na tela inicial com o título **You are Jailbroken**

####  Aplicando o hotfix

O hotfix evita que o jailbreak se perca quando o Kindle for atualizado. De qualquer forma, é sempre bom deixar o Kindle em modo avião a não ser que você precise de internet, pra evitar que ele seja atualizado sozinho e tudo quebre.

 - Baixe o [arquivo do hotfix](https://gitlab.com/filipekiss/kindle/raw/master/jailbreak/JailBreak-1.14.N-FW-5.x-hotfix.zip)
 - Descompacte o arquivo
 - Conecte seu Kindle via USB
 - Copie o arquivo `Update_jailbreak_hotfix_1.14.N_install.bin` pra raiz do seu Kindle
 - Apague o arquivo `main-htmlviewer.tar.gz`
 - Desconecte o Kindle (Ejetar/Remover com segurança)
 - Na tela inicial **[Menu]** > **[Configurações]** > **[Menu]** > **[Atualizar seu Kindle]** > **[Ok]**
 - Aguarde a atualização

#### Instalando o KUAL e o MRPI

O Kual (Kindle Universal Application Launcher) é o responsável por permitir que você rode as extensões do Jailbreak no Kindle. O MRPI (MobileRead Package Installer) é uma extensão do qual necessária pra instalar outras extensões. A instalação é bem simples:

 - Baixe o [KUAL](https://gitlab.com/filipekiss/kindle/raw/master/kual/KUAL-v2.7.zip)
 - Extraia o arquivo `KUAL-v2.7.zip`
 - Copie o arquivo `KUAL-KDK-2.0.azw2` pra dentro da pasta `documents` do seu Kindle
 - Baixe o [MRPI](https://gitlab.com/filipekiss/kindle/raw/master/kual/extensions/kual-mrinstaller-1.6.N.zip)
 - Descompacte o arquivo `kual-mrinstaller-1.6.N.zip` e copie o conteúdo da pasta pra raiz do seu kindle
 - Desconecte seu Kindle (Ejetar/Remover com segurança)
 - Na home, procure o "livro" chamado **Kindle Launcher**. Abra-o.
 
 Uma lista com duas opções (**KUAL** e **Helper**) deve aparecer. Agora é só instalarmos o pacote dos screensavers.
 
####  Instalando e configurando o Linkss

O Linkss é o pacote responsável por gerar os wallpapers da capa do livro que você está lendo no momento. Vale lembrar que a geração da capa pode demorar um pouco depois de você começar a leitura de um livro.

 - Baixe o [Python](https://gitlab.com/filipekiss/kindle/raw/master/kual/extensions/kindle-python-0.14.N-pw2_kt2_kv_pw3_koa_kt3.zip)
 - Baixe o [Linkss](https://gitlab.com/filipekiss/kindle/raw/master/kual/extensions/kindle-linkss-0.24.N.zip)
 - Copie o arquivo `Update_python_0.14.N_install_pw2_kt2_kv_pw3_koa_kt3.bin` do Python e o arquivo `Update_linkss_0.24.N_install_pw2_kt2_kv_pw3_koa_kt3.bin` para o seu Kindle, dentro da pasta `mrpackages`
 - Abra o KUAL no seu Kindle > **[Helper]** > **[Install MR Packages]** 
 - O KUAL vai instalar os pacotes e reiniciar seu Kindle. Abra o KUAL e uma nova opção vai aparecer no menu.

O Linkss tem três opções: Última tela, Capa do Livro atual e Imagens personalizadas.

##### Capa do Livro

![](images/cover.gif)

O modo que usa a capa do livro como protetor de tela do Kindle funciona de um jeito simples: quando você abre um livro, o Linkss identifica o livro que você está lendo, lê as informações do próprio arquivo e gera uma capa de acordo com o livro que você está lendo. Vale lembrar que o processamento do Kindle é um pouco limitado, então a geração da capa pode demorar alguns minutos após você começar a ler. 

Para ativar, abra o KUAL > **[Screen Savers]** > **[Screen Savers Behavior]** > **[Cover]** > **[Screen Savers]** > **[Restart framework now]**


##### Última tela

![](images/overlay.jpg)

O Última tela basicamente vai gerar uma imagem da última tela mostrada no seu Kindle. Assim você pode deixar a última página e fica fácil de lembrar onde você parou no livro. O próprio Linkss vai adicionar uma marca pra avisar que o seu Kindle está dormindo pra você não tentar virar a página com ele travado. Pra ativar esse modo, abra KUAL > **[Screen Savers]** > **[Screen Savers Behavior]** > **[Last Screen]** > **[Screen Savers]** > **[Restart framework now]**


##### Imagens Personalizadas

![](images/custom.gif)

Deixei a opção de imagens personalizadas por último pois ela é um pouquinho mais complicada. Ela é a opção que vem ativada por padrão quando o Linkss é instalado, então você não precisa fazer nada de especial. As limitações existem, na verdade, quando você precisa criar a imagem. O Kindle é bem chato com o formato das imagens a serem utilizadas como Wallpaper, conforme a tabela abaixo.

| Nickname | Modelo                   | Tamanho do Wallpaper |
|----------|--------------------------|----------------------|
| KOA      | Kindle Oasis             | 1072x1448            |
| KV       | Kindle Voyager           | 1072x1448            |
| PW3      | Kindle Paperwhite (2015) | 1072x1448            |
| PW2      | Kindle Paperwhite        | 758x1024             |
| KT3      | Kindle Basic 2           | 600x800              |
| KT2      | Kindle Basic             | 600x800              |

Além de tudo, a imagem precisa ser exportada no formato PNG, ter o nome somente em letras minúsculas e devem ser preferencialmente em escala de cinza. O próprio Linkss vai validar as imagens quando você reiniciar o framework (toda vez que você colocar imagens novas você precisar reiniciar o framework **[KUAL]** > **[Screen Savers]** > **[Restart framework now]**).

## Considerações finais

Apesar de ser um processo que precisa ser feito com cuidado, o jailbreak é simples e não afeta a experiência padrão do Kindle. A extensão das capas, por exemplo, mesmo que ela quebre (na hora de gerar uma capa ou na hora de tentar validar as imagens que você colocou na sua pasta de screensavers) não vai deixar seu Kindle inutilizado. Eu não senti nenhuma degradação de performance e meu kindle funciona perfeitamente, inclusive pra receber arquivos pelo e-mail. Se você tiver alguma dúvida, me manda uma mensagem no [twitter][twitter-intent] ou [abra uma issue](https://gitlab.com/filipekiss/kindle/issues/new) que eu vou tentar te ajudar. :)

Abraços!

## Créditos e agradecimentos

 - [**knc1** pelo _Post original do Mobile Read_](http://www.mobileread.com/forums/showthread.php?t=275877)

 - [**NiLuje** pela _extensão do Linkss_](http://www.mobileread.com/forums/showthread.php?t=195474)
 - [**Carol**, por ter arriscado que eu _brickasse_ o Kindle dela](http://twitter.com/csacerdote)




 


[twitter-book-cover]: https://twitter.com/filipekiss/status/784099154180136960
[twitter-fixed-images]: https://twitter.com/filipekiss/status/784498784508243968
[ygor]: https://twitter.com/ygorch/status/784099433667502080
[barbara]: https://twitter.com/Barbara_G_Rocha/status/784500230976835586
[filipegamer]: https://twitter.com/FilipeGAMER/status/784125923712958467
[centralheaven]: https://twitter.com/centralheaven/status/784500361121959937
[analuiza]: https://twitter.com/freeblowjob/status/784202614237958145
[csacerdote]: https://twitter.com/csacerdote
[kindle-model-id]: http://wiki.mobileread.com/wiki/Kindle_Serial_Numbers
[kindled]: https://filipekiss.com.br/kindle
[twitter-intent]: https://twitter.com/intent/tweet?text=@filipekiss&